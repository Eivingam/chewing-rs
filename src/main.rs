use chewing_rs::Chewing;

fn main() {
    let chw = Chewing::new().unwrap();
    chw.config();
    //ㄎㄨ
    chw.default('x'); //ㄎ
    chw.default('m'); //ㄨ
    chw.default('4');
    chw.default('t');
    chw.default('8');
    chw.default('6');
    chw.capslock();
    chw.default('j');
    chw.capslock();
    // chw.default('t');
    // chw.default('8');
    chw.default('t');
    chw.default('8');
    chw.default('6');
    //chw.enter();

    let buffer = chw.buffer();

    let bopomofo = chw.bopomofo();

    // chw.left();
    // chw.left();
    // chw.left();
    // chw.left();
    // chw.left();
    chw.down();
    let selection = chw.list();
    chw.choose_by_index(20);

    //println!("{:?}", s.include_paths);
    //println!("ㄎㄨ");
    println!("{}{}", buffer, bopomofo);
    println!("{:?}", selection);
}
