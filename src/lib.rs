use chewing_sys as chewing;
use failure::{bail, Error};
use std::ffi::CStr;

pub struct Chewing {
    // This pointer must never be allowed to leave the struct
    ctx: *mut chewing::ChewingContext,
    mode: u32
}

impl Chewing {
    pub fn new() -> Result<Self, Error> {
        let ctx = unsafe { chewing::chewing_new() };
        // Manually handle null pointer returns here
        if ctx.is_null() {
            bail!("chewing_new() failed");
        }

        unsafe { chewing::chewing_set_ChiEngMode(ctx, chewing::CHINESE_MODE as i32) };
        Ok(Chewing { ctx, mode :chewing::CHINESE_MODE })
    }

    pub fn config(&self) {
        unsafe {
            chewing::chewing_set_candPerPage(self.ctx, 10);
            chewing::chewing_set_KBType(self.ctx, 0);
            chewing::chewing_set_ShapeMode(self.ctx, 0);
            chewing::chewing_set_addPhraseDirection(self.ctx, 1);
            chewing::chewing_set_autoShiftCur(self.ctx, 1);
            chewing::chewing_set_easySymbolInput(self.ctx, 0);
            chewing::chewing_set_escCleanAllBuf(self.ctx, 1);
            chewing::chewing_set_phraseChoiceRearward(self.ctx, 1);
            chewing::chewing_set_spaceAsSelection(self.ctx, 1);
        }
    }

    pub fn chinese_or_english_mode(&self){
        if self.mode == chewing::CHINESE_MODE {
            unsafe { chewing::chewing_set_ChiEngMode(self.ctx, chewing::SYMBOL_MODE as i32)}
        } else {
            unsafe { chewing::chewing_set_ChiEngMode(self.ctx, chewing::CHINESE_MODE as i32)}
        }
    }

    pub fn symbol_length(&self, symbols: u8){
        unsafe {
            chewing::chewing_set_maxChiSymbolLen(self.ctx, symbols as i32);
        }
    }

    pub fn list(&self) -> Vec<&str> {
        unsafe {
            chewing::chewing_cand_Enumerate(self.ctx);
            let mut selection = Vec::new();
            while chewing::chewing_cand_hasNext(self.ctx) > 0 {
                selection.push(
                    CStr::from_ptr(chewing::chewing_cand_String_static(self.ctx))
                        .to_str()
                        .expect("Cstr from selection failed"),
                );
            }
            selection
        }
    }

    pub fn choose_by_index(&self, index: u8) {
        unsafe {
            chewing::chewing_cand_choose_by_index(self.ctx, index as i32);
        }
    }

    pub fn buffer(&self) -> &str {
        unsafe {
            let buf = chewing::chewing_buffer_String_static(self.ctx);
            let str = CStr::from_ptr(buf).to_str().expect("Cstr from buf failed");
            str
        }
    }

    pub fn bopomofo(&self) -> &str {
        unsafe {
            let buf = chewing::chewing_bopomofo_String_static(self.ctx);
            let str = CStr::from_ptr(buf).to_str().expect("Cstr from buf failed");
            str
        }
    }

    pub fn backspace(&self) {
        unsafe {
            chewing::chewing_handle_Backspace(self.ctx);
        }
    }

    pub fn capslock(&self) {
        unsafe {
            chewing::chewing_handle_Capslock(self.ctx);
        }
    }

    pub fn ctrl_num(&self, key: char) {
        unsafe {
            chewing::chewing_handle_CtrlNum(self.ctx, key as i32);
        }
    }

    pub fn double_tab(&self) {
        unsafe {
            chewing::chewing_handle_DblTab(self.ctx);
        }
    }

    pub fn default(&self, key: char) {
        unsafe {
            chewing::chewing_handle_Default(self.ctx, key as i32);
        }
    }

    pub fn del(&self) {
        unsafe {
            chewing::chewing_handle_Del(self.ctx);
        }
    }

    pub fn down(&self) {
        unsafe {
            chewing::chewing_handle_Down(self.ctx);
        }
    }

    pub fn end(&self) {
        unsafe {
            chewing::chewing_handle_End(self.ctx);
        }
    }

    pub fn enter(&self) {
        unsafe {
            chewing::chewing_handle_Enter(self.ctx);
        }
    }

    pub fn esc(&self) {
        unsafe {
            chewing::chewing_handle_Esc(self.ctx);
        }
    }

    pub fn home(&self) {
        unsafe {
            chewing::chewing_handle_Home(self.ctx);
        }
    }

    pub fn left(&self) {
        unsafe {
            chewing::chewing_handle_Left(self.ctx);
        }
    }

    pub fn num_lock(&self, key: char) {
        unsafe {
            chewing::chewing_handle_Numlock(self.ctx, key as i32);
        }
    }

    pub fn page_up(&self) {
        unsafe {
            chewing::chewing_handle_PageUp(self.ctx);
        }
    }

    pub fn page_down(&self) {
        unsafe {
            chewing::chewing_handle_PageDown(self.ctx);
        }
    }

    pub fn right(&self) {
        unsafe {
            chewing::chewing_handle_Right(self.ctx);
        }
    }

    pub fn shift_left(&self) {
        unsafe {
            chewing::chewing_handle_ShiftLeft(self.ctx);
        }
    }

    pub fn shift_right(&self) {
        unsafe {
            chewing::chewing_handle_ShiftRight(self.ctx);
        }
    }

    pub fn shift_space(&self) {
        unsafe {
            chewing::chewing_handle_ShiftSpace(self.ctx);
        }
    }

    pub fn space(&self) {
        unsafe {
            chewing::chewing_handle_Space(self.ctx);
        }
    }

    pub fn tab(&self) {
        unsafe {
            chewing::chewing_handle_Tab(self.ctx);
        }
    }

    pub fn up(&self) {
        unsafe {
            chewing::chewing_handle_Up(self.ctx);
        }
    }
}

impl Drop for Chewing {
    fn drop(&mut self) {
        unsafe { chewing::chewing_delete(self.ctx) }
    }
}
